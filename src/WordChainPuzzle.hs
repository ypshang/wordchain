-- By Yuping Shang, May 2016
-- Word chains computation from a given starting word, to a given target word of the same length
-- based on the given dictionary file "four_letter_words.txt"

import System.IO.Unsafe
import Data.List
import Control.Monad
import Data.Tree

-- Get a list of dictionary meaningful adjoined wors from a given word.
-- And filter out the word(s) which is/are not found in the dictionary
wordChains :: String -> [String]
wordChains word = filter (\w -> elem w dictionary) . neighbours $ word

-- The dictionary read from file "four_letter_words.txt" splited into [String]
dictionary :: [String]
dictionary = concat $ liftM words $ lines $ unsafePerformIO (readFile "four_letter_words.txt")

-- Make neighbour words(may not included in the dictionary) to the given word
neighbours :: String -> [String]
neighbours [] = []
neighbours (w:ws) = [w':ws | w' <- ['A'..'Z'], w' /= w] ++ [w:ws' | ws' <- neighbours ws]

-- Make a multi-way tree as the neighbour word might be multiple
-- http://haddock.stackage.org/lts-5.15/containers-0.5.6.2/Data-Tree.html
mkTree :: (w -> [w]) -> w -> Tree w
mkTree childrenOf w = Node w [mkTree childrenOf w' | w' <- childrenOf w]

-- Breadth First is going to be applied to get the adjoined words, then next adjoined, then next next
-- https://en.wikipedia.org/wiki/Breadth-first_search
-- referred from https://blogs.msdn.microsoft.com/matt/2008/05/11/breadth-first-tree-traversal-in-haskell/
breadthFirst :: Tree w -> [w]
breadthFirst w = breadthFirst' [w]
breadthFirst' :: [Tree w] -> [w]
breadthFirst' trees = [ w | Node w _ <- trees ] ++ breadthFirst' (concat $ map subForest trees)

-- Query and filter to reach the targeted list of words from the tree made
bfSearch :: (w -> Bool) -> (w -> [w]) -> w -> [w]
bfSearch isTarget childrenOf = filter isTarget . breadthFirst . mkTree childrenOf

-- Generate whole chained words list from one list of words to their neighour's, to next neighour's... with for comprehension
chainPaths :: [String] -> [[String]]
chainPaths (w:ws) = [ chanWrd : w : ws | chanWrd <- wordChains w, chanWrd `notElem` ws ]

goal :: String -> [String] -> Bool
goal target ws = target == head ws

-- Word Chain Path completation.
-- input: a is the source word, b is the targetd word
-- output: Chain pathes computed.
--
-- From ghci, under fold with the files four_letter_words.txt and WordChainPuzzle.hs located,
-- prelude>:l WordChainPuzzle.hs
-- prelude>wordChainPath "HATE" "LOVE"
-- The result to be:
-- ["HATE","LATE","LOTE","LOVE"]
wordChainPath a b = reverse . concat . take 1 $ bfSearch (goal b) chainPaths [a]
