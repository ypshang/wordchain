# README #

## Problem
>A word chain is a sequence of words from a given starting word, to a given target word of the same length, where each step in the chain is a legal word that can only change one letter from the word that precedes it. The legal words can be presumed to have been supplied in a simple lexicon file and are all 4 letters long. 
For the purposes of this exercise we can consider the Lexicon to contain the list of all the legal (i.e. usable in Scrabble) 4 letter English words, presented in lexicographical order. 
 
>The required solution is a program that produces a list of intermediate words between the starting and target words in the chain i.e. step1 and step2 in the diagram below. The best solution produces the shortest word chains. 
 
>An illustration for a very simple example is shown below.

>HATE --> HAVE --> HOVE --> LOVE